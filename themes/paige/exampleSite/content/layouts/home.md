---
authors: ["will_faught"]
categories: ["layouts", "paige"]
description: "Demonstration of the Paige home layout."
layout: "paige/home"
paige:
  home:
    blurb: "This is the blurb."
    greeting: "This is the greeting"
    image:
      raw: false
      stretch: false
      url: "landscape.webp"
  main:
    metadata:
      authors:
        hide: true
      date:
        hide: true
      reading_time:
        hide: true
      terms:
        hide: true
      title:
        class: "display-1 fw-bold text-center"
  social:
    discord:
      bootstrap_icon: "discord"
      name: "Discord"
      url: "https://discord.com/users/username"
    dribbble:
      bootstrap_icon: "dribbble"
      name: "Dribbble"
      url: "https://dribbble.com/username"
    email:
      bootstrap_icon: "envelope"
      name: "E-mail"
      url: "mailto:username@example.com"
    facebook:
      bootstrap_icon: "facebook"
      name: "Facebook"
      url: "https://facebook.com/username"
    github:
      bootstrap_icon: "github"
      name: "GitHub"
      url: "https://github.com/username"
    instagram:
      bootstrap_icon: "instagram"
      name: "Instagram"
      url: "https://instagram.com/username"
    linkedin:
      bootstrap_icon: "linkedin"
      name: "LinkedIn"
      url: "https://linkedin.com/in/username"
    medium:
      bootstrap_icon: "medium"
      name: "Medium"
      url: "https://medium.com/@username"
    pinterest:
      bootstrap_icon: "pinterest"
      name: "Pinterest"
      url: "https://pinterest.com/username"
    quora:
      bootstrap_icon: "quora"
      name: "Quora"
      url: "https://quora.com/profile/username"
    reddit:
      bootstrap_icon: "reddit"
      name: "Reddit"
      url: "https://reddit.com/u/username"
    spotify:
      bootstrap_icon: "spotify"
      name: "Spotify"
      url: "https://open.spotify.com/user/username"
    stackoverflow:
      bootstrap_icon: "stack-overflow"
      name: "Stack Overflow"
      url: "https://stackoverflow.com/users/username"
    strava:
      bootstrap_icon: "strava"
      name: "Strava"
      url: "https://strava.com/athletes/username"
    twitch:
      bootstrap_icon: "twitch"
      name: "Twitch"
      url: "https://twitch.tv/username"
    twitter:
      bootstrap_icon: "twitter"
      name: "Twitter"
      url: "https://twitter.com/username"
    vimeo:
      bootstrap_icon: "vimeo"
      name: "Vimeo"
      url: "https://vimeo.com/username"
    wordpress:
      bootstrap_icon: "wordpress"
      name: "Wordpress"
      url: "https://username.wordpress.com"
    yelp:
      bootstrap_icon: "yelp"
      name: "Yelp"
      url: "https://yelp.com/user_details?userid=username"
    youtube:
      bootstrap_icon: "youtube"
      name: "YouTube"
      url: "https://youtube.com/user/username"
tags: ["home"]
title: "Home Layout"
weight: 10
---

Paige provides a `paige/home` layout for a home page.

<!--more-->

These are the parameters for this page:

```yaml
---
authors: ["will_faught"]
categories: ["layouts", "paige"]
description: "Demonstration of the Paige home layout."
layout: "paige/home"
paige:
  home:
    blurb: "This is the blurb."
    greeting: "This is the greeting"
    image:
      raw: false
      stretch: false
      url: "landscape.webp"
  main:
    metadata:
      authors:
        hide: true
      date:
        hide: true
      reading_time:
        hide: true
      terms:
        hide: true
      title:
        class: "display-1 fw-bold text-center"
  social:
    discord:
      bootstrap_icon: "discord"
      name: "Discord"
      url: "https://discord.com/users/username"
    dribbble:
      bootstrap_icon: "dribbble"
      name: "Dribbble"
      url: "https://dribbble.com/username"
    email:
      bootstrap_icon: "envelope"
      name: "E-mail"
      url: "mailto:username@example.com"
    facebook:
      bootstrap_icon: "facebook"
      name: "Facebook"
      url: "https://facebook.com/username"
    github:
      bootstrap_icon: "github"
      name: "GitHub"
      url: "https://github.com/username"
    instagram:
      bootstrap_icon: "instagram"
      name: "Instagram"
      url: "https://instagram.com/username"
    linkedin:
      bootstrap_icon: "linkedin"
      name: "LinkedIn"
      url: "https://linkedin.com/in/username"
    medium:
      bootstrap_icon: "medium"
      name: "Medium"
      url: "https://medium.com/@username"
    pinterest:
      bootstrap_icon: "pinterest"
      name: "Pinterest"
      url: "https://pinterest.com/username"
    quora:
      bootstrap_icon: "quora"
      name: "Quora"
      url: "https://quora.com/profile/username"
    reddit:
      bootstrap_icon: "reddit"
      name: "Reddit"
      url: "https://reddit.com/u/username"
    spotify:
      bootstrap_icon: "spotify"
      name: "Spotify"
      url: "https://open.spotify.com/user/username"
    stackoverflow:
      bootstrap_icon: "stack-overflow"
      name: "Stack Overflow"
      url: "https://stackoverflow.com/users/username"
    strava:
      bootstrap_icon: "strava"
      name: "Strava"
      url: "https://strava.com/athletes/username"
    twitch:
      bootstrap_icon: "twitch"
      name: "Twitch"
      url: "https://twitch.tv/username"
    twitter:
      bootstrap_icon: "twitter"
      name: "Twitter"
      url: "https://twitter.com/username"
    vimeo:
      bootstrap_icon: "vimeo"
      name: "Vimeo"
      url: "https://vimeo.com/username"
    wordpress:
      bootstrap_icon: "wordpress"
      name: "Wordpress"
      url: "https://username.wordpress.com"
    yelp:
      bootstrap_icon: "yelp"
      name: "Yelp"
      url: "https://yelp.com/user_details?userid=username"
    youtube:
      bootstrap_icon: "youtube"
      name: "YouTube"
      url: "https://youtube.com/user/username"
tags: ["home"]
title: "Home Layout"
weight: 10
---
```
