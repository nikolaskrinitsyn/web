---
description: "Powerful, pliable pixel perfection."
layout: "paige/home"
paige:
  home:
    blurb: "Paige is designed to put your content front and center, avoiding the typical clutter. The look is seamless and smooth, scalable and readable, portable and efficient. The layout is minimal and responsive, using verticality and white space to focus and delineate parts of the page. The implementation is flexible and extensible. It's a versatile canvas that serves most web needs."
    greeting: "An advanced Hugo theme"
    image:
      stretch: true
      url: "landscape.webp"
  social:
    github:
      bootstrap_icon: "github"
      name: "GitHub"
      url: "https://github.com/willfaught/paige"
  style: ".paige-title { font-size: 5rem; }"
title: "Paige"
---

<a class="lead" href="https://github.com/willfaught/paige">Install it now.</a>
